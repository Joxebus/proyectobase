$(document).ready(function() {
    $("#confirmarNueva").keyup(function(){
        $.ajax({
            type: "POST",
            url: "${createLink(action: 'validaContrasenia', controller:'usuario')}",
            data: {
                'contraseniaNueva': $("#contraseniaNueva").val(),
                'confirmarNueva': $("#confirmarNueva").val() // <-- the $ sign in the parameter name seems unusual, I would avoid it
            },
            success : function(response) {
                if(response.exito){
                    $("#error").hide()
                    $("#mensaje").show()
                    $("#aceptar").removeAttr("disabled");
                    $("#mensaje").html(response.exito)
                }else{
                    $("#error").show()
                    $("#mensaje").hide()
                    $("#aceptar").attr("disabled", "disabled");
                    $("#error").html(response.error)
                }
            }
        });
    });
});