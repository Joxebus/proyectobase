package proyecto.base.util

class NotificacionesService {

    def grailsApplication
    private EmailUtils emailUtils = new EmailUtils()

    def enviarCorreoHtml(List<String> destinatarios, String asunto, String mensajeHtml) {
        emailUtils.subject = asunto
        emailUtils.from = grailsApplication.config.mail.default.no.reply
        emailUtils.contentHtml = mensajeHtml
        emailUtils.recipients = destinatarios
        emailUtils.sendEmail();
    }
}
