package proyecto.base.util

import proyecto.base.exception.ArchivoException
import proyecto.base.files.Archivo

class ManejoArchivosService {

    def grailsApplication
    def validacionTipoArchivoService

    def guardarArchivo (params) throws ArchivoException {
        File ubicacionLocal = new File(grailsApplication.config.local.url.files)
        String ubicacionLocalString = grailsApplication.config.local.url.files
        String nombreCompleto = params.archivo?.getFileItem().getName() // trae el nombre del archivo

        log.debug "Guardando en file system: $nombreCompleto"
        if(params.archivo && params.archivo.getBytes().length > grailsApplication.config.local.max.size.files){
            log.error "El tamaño maximo por archivo es de ${grailsApplication.config.local.max.size.files / 1048576} MB"
            throw ArchivoException("El tamaño maximo por archivo es de ${grailsApplication.config.local.max.size.files / 1048576} MB")
        }

        if(params.subcarpetas){
            ubicacionLocal = new File(ubicacionLocal+params.subcarpetas)
            ubicacionLocalString = ubicacionLocal+params.subcarpetas+'/'
        }

        Archivo archivo = new Archivo()

        archivo.nombre = nombreCompleto.substring(0, nombreCompleto.lastIndexOf("."))
        archivo.extension = nombreCompleto.substring(nombreCompleto.lastIndexOf(".")+1)
        archivo.contentType = validacionTipoArchivoService.findMime(nombreCompleto.substring(nombreCompleto.lastIndexOf('.')+1))
        archivo.ubicacionLocal = ubicacionLocalString+archivo.toString()
        archivo.tamano = params.archivo?.getBytes().length

        if(!ubicacionLocal.exists()){
           ubicacionLocal.mkdirs()
        }
        if(!archivo.save()){
            log.error "No se pudo guardar el archivo"
            archivo.errors.each{
                log.error it
            }
            throw new ArchivoException("No se pudo guardar el archivo porque contiene errores: "+archivo.errors)

        }else{
            try{
                FileOutputStream archivoOut = new FileOutputStream(archivo.ubicacionLocal)
                archivoOut.write(params.archivo.bytes)
                log.debug "Se guardo correctamente el archivo ${archivo.ubicacionLocal}"
            }catch(Exception e){
                log.error "No se pudo guardar el archivo ${e.message}"
                archivo.delete()
                throw new ArchivoException("No se pudo guardar el archivo: "+e.message)
            }
        }
        return archivo
    }

    def eliminarArchivo(params) {
        Archivo archivo = Archivo.get(params.idArchivo)
        String nombreArchivo = archivo.toString()
        log.debug "Obteniendo de file system: ${archivo.ubicacionLocal}"
        File archivoIn = new File(archivo.ubicacionLocal)
        log.debug "Eliminando archivo del sistema"
        archivoIn.delete()
        archivo.delete()
        return nombreArchivo

    }

    def obtenerArchivo(params) {
        Archivo archivo = Archivo.get(params.idArchivo)
        log.debug "Obteniendo de file system: ${archivo.ubicacionLocal}"
        return new File(archivo.ubicacionLocal)
    }
}
