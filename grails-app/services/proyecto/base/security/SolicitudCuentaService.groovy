package proyecto.base.security

class SolicitudCuentaService {

    static transactional = false

	def getPaginaUsuarios(inicioPagina, tamanioPagina) {
		def max = Math.min(inicioPagina ? Integer.valueOf(inicioPagina): 10, 10)
		def offset = Math.min(tamanioPagina ? Integer.valueOf(tamanioPagina) : 0,10000)
        def usuariosList = UsuarioSistema.findAllByEstadoLogicoNotEqual(3,[max, offset])
        def usuariosTotal = usuariosList.size()
        [usuariosList: usuariosList, usuariosTotal: usuariosTotal]
	}

	def getPaginaUsuariosPorNombre(inicioPagina, tamanioPagina, nombre) {
		def max = Math.min(inicioPagina ? Integer.valueOf(inicioPagina): 10, 10)
		def offset = Math.min(tamanioPagina ? Integer.valueOf(tamanioPagina) : 0,10000)
		def usuariosList = UsuarioSistema.findAllByNombre(nombre, [max, offset])
		[usuariosList:usuariosList, usuariosTotal:usuariosList.size()]
	}


	def getPaginaUsuariosPorUsername(inicioPagina, tamanioPagina, username) {
		def max = Math.min(inicioPagina ? Integer.valueOf(inicioPagina): 10, 10)
		def offset = Math.min(tamanioPagina ? Integer.valueOf(tamanioPagina) : 0,10000)
		def usuariosList = UsuarioSistema.findAllByUsername(username, [max, offset])
		[usuariosList:usuariosList, usuariosTotal:usuariosList.size()]
	}

}
