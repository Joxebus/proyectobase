package proyecto.base.security

import grails.gsp.PageRenderer
import org.springframework.dao.DataIntegrityViolationException
import org.springframework.web.servlet.ModelAndView
import proyecto.base.util.CriptoUtils
import proyecto.base.util.EmailUtils

class SolicitudCuentaController {

    def grailsApplication
    def notificacionesService
    def springSecurityService
    def loginService
    def solicitudCuentaService
    def defaultViewSolicitudCuenta = "/login/SolicitudCuenta"

    static defaultAction = "list"

    static valorDelParametro
    static valorFiltro = 1

    private EmailUtils emailUtils = new EmailUtils()
    private CriptoUtils criptoUtils = new CriptoUtils()

    PageRenderer groovyPageRender = new PageRenderer()

    def list = {
        valorFiltro = 1
        params.max = Math.min(params.max ? params.int('max') : 5, 50)
        if (!params.sort)
            params.sort = "id"
        if (!params.order)
            params.order = "desc"

        def usuariosList = UsuarioSistema.findAllByEstadoLogicoNotEqual(3, params)
        def usuariosTotal = UsuarioSistema.countByEstadoLogicoNotEqual(3)

        render(view: '/login/listadoSolicitudes',
                model: [usuarioSistemaInstanceList: usuariosList, usuarioSistemaInstanceTotal: usuariosTotal])
    }

    def showTemplateRol() {
        log.debug "Mostrando formulario de asignacion de rol para el usuario ${params.id}"
        render template:'/usuarioSistema/asignarRol',
               model:[usuarioSistemaInstance:UsuarioSistema.load(params.id)]
    }

    def asignarRol(){
        try{

        UsuarioSistema usuario = UsuarioSistema.get(params.id)
        Permiso permiso = Permiso.read(params.rol)
        String password = criptoUtils.generarContrasenia(8)

        String subject = grailsApplication.config.mail.default.subject.activate
        String contentHtml = groovyPageRender.render(template:'/mail/activarUsuario',
                model:[username:usuario.username, password:password])
        List<String> recipients = [usuario.username]
        if(!subject || !contentHtml || !recipients){
            log.error "No se puede enviar la notificacion "
            flash.error = "No se puede enviar la notificacion "
        }else{
            usuario.password = password
            usuario.tipoUsuario = permiso.tipoUsuario
            usuario.enabled = true
            usuario.save()
            UsuarioSistemaPermiso.create(usuario, permiso, true)
            notificacionesService.enviarCorreoHtml(recipients, subject, contentHtml)
            flash.message = "Se activo el usuario ${usuario.username} contraseña ${password}"
        }

        }catch(Exception e){
            flash.error = "No se pudo enviar la notificacion por el siguiente error "+e.message
        }
        redirect(action:'list')
    }

    /*def asignarRol = {
        if (!params.role) {
            session.rolesPendientes = null
        } else {
            session.rolesPendientes += [("${params.id}"): params.role, idUsuario: params.id]
            def model = solicitudCuentaService.getPaginaUsuarios(params.max, params.offset)
            model += [criterio: "-Seleccione un criterio-"]
            model += [valorCriterio: ""]
            model += [rol: params.role]
            log.info "Almacenando en memoria el rol asignado al idUsuario: ${params.id} - rol: ${params.role}"
            return new ModelAndView(this.defaultViewSolicitudCuenta, model)
        }
    } */


    def resumenActivaUsuario = {
        if(!session.rolesPendientes){
            flash.warning = message(code: 'usuario.sistema.seleccion.rol')
            redirect(action: 'list')
        } else {
            if (!params.id.equals(session.rolesPendientes.get("idUsuario"))) {
                session.rolesPendientes = null
                flash.warning = message(code: 'usuario.sistema.seleccion.rol')
                redirect(action: 'list')
            } else {
                def model = [:]
                String password = this.criptoUtils.getNuevaContrasenia()
                def usuario = UsuarioSistema.get(params.id)
                model += [idUsuario:params.id]
                model += [contrasenia:password]
                model += [email:usuario.email]
                return new ModelAndView("/login/CreacionCuentaUsuario", model)
            }
        }
    }

    def activaUsuario = {
        log.info('ACTIVACION USUARIO')
        log.info('UsuarioAc:' + params.id)
        def model = [:]

        def usuarioSistema = UsuarioSistema.findById(usuario.usuarioSistema.id)

        def rol = session.rolesPendientes.get("${params.id}")

        log.info("Enviando notificacion a el usuario: ${params.email}")
        this.emailUtils.subject = grailsApplication.config.mail.default.subject.activate
        this.emailUtils.from = grailsApplication.config.mail.default.no.reply //"admin@citec.com"
        this.emailUtils.contentHtml = groovyPageRender.render(template:'/mail/activarUsuario',
                model:[username:usuarioSistema.username, password:params.contrasena])//this.getHtmlActivar(usuarioSistema, params.contrasenia)
        this.emailUtils.recipients = ["${params.email}".trim()]
        this.emailUtils.sendEmail();

        //Hasta aqu�

        if (rol == null) {
            usuarioSistema.password = params.contrasenia
            usuarioSistema.enabled = true
            usuarioSistema.save(flush: true)
            log.info "El usuario ${usuarioSistema.nombre} fue re-activado completamente"
        } else {
            def role = Permiso.get(rol)
            usuarioSistema.password = params.contrasenia
            usuarioSistema.enabled = true
            usuarioSistema.save(flush: true)

            UsuarioSistemaPermiso.create usuarioSistema, role, true
            log.info "El usuario ${usuarioSistema.nombre} fue activado completamente con el rol ${role.authority}"
        }
        session.rolesPendientes = null
        return new ModelAndView("/login/ActivacionExitosa", model)
    }

    def cancelar(){
        session.rolesPendientes = null
    }

    def instruccionesToolTip = {
        render """
		Es la observaci\u00F3n que se requiere<br>
		poner, por ejemplo: El puesto:<br>
		Director General
		"""
    }


    def edit(Long id) {
        def usuario = UsuarioSistema.get(id)
        def rolUsuario = UsuarioSistemaPermiso.findByUsuarioSistema(usuario)
        render(view: '/login/edit', model: [usuario:usuario, rolUsuario:rolUsuario])
    }

    def updateUsuario (Long id, Long version) {
        def usuarioSistema = UsuarioSistema.get(id)


        if(!params.sort)
            params.sort= "id"
        if(!params.order)
            params.order= "desc"

        def usuariosList = UsuarioSistema.findAllByEstadoLogicoNotEqual(3, params)
        def usuariosTotal = usuariosList.size()

        if (!usuarioSistema) {
            flash.warning = "El usuario no existe"
            redirect(action: 'list')
        }

        if (version != null) {
            if (usuarioSistema.version > version) {
                flash.warning = "El usuario no existe"
                redirect(action: 'list')
            }
        }


        usuarioSistema.username = params.username
        usuarioSistema.nombre = params.nombre
        usuarioSistema.email = params.email

        if (!usuarioSistema.save(flush: true)) {

            usuarioSistema.refresh()
            usuarioSistema.errors

            render(view: '/login/SolicitudCuenta',
                    model: [usuarioSistema:usuarioSistema,
                            usuariosList: usuariosList,
                            usuariosTotal: usuariosTotal])

        } else {
            if (params.rol) {
                def rol = Permiso.findById(params.rol)
                UsuarioSistemaPermiso.executeUpdate(
                        "update UsuarioSistemaPermiso set permiso=:rol where usuarioSistema=:usuarioSistema",
                        [rol:rol, usuarioSistema: usuarioSistema])
            }
            flash.success = "El usuario ${usuarioSistema.username} se actualizo correctamente"
            redirect(action: 'list')
        }
    }

    //********************** EL DE BORRAR **************************

    def delete(Long id) {
        def usuario = UsuarioSistema.get(id)


        if (!usuario) {
            flash.erroMessage = 'No se encontr\u00F3 el usuario '
            redirect(action: "list")

        }//fin del if

        try {
            usuario.estadoLogico = 3

            usuario.enabled = false
            usuario.estadoLogico = 3

            if (UsuarioSistemaPermiso.getByID(usuario.id)) {
                UsuarioSistemaPermiso.executeUpdate(
                        "delete from UsuarioSistemaPermiso where usuarioSistema=:usuarioSistema",
                        [usuarioSistema: usuario])
            }

            usuario.save(flus: true)

            flash.success = "Usuario Eliminado Correctamente"
            redirect(action: 'list')
        }
        catch (DataIntegrityViolationException e) {
            flash.errorMessage = ' El usuario no se pudo eliminar'
            redirect(action: "list")
        }//fin del catch


    }//fin de delete


}//fin de la clase
