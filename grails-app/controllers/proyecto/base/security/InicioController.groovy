package proyecto.base.security



class InicioController {

    def springSecurityService
    def validacionTipoArchivoService

    def beforeInterceptor = {
        if (!springSecurityService.isLoggedIn()) {
            redirect controller: "login", action: 'auth', params: params
            return false
        }
    }

    def index() {
        session.usuario = springSecurityService.currentUser
    }


}
