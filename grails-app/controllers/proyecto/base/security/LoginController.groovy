package proyecto.base.security

import grails.converters.JSON
import grails.gsp.PageRenderer
import org.codehaus.groovy.grails.plugins.springsecurity.SpringSecurityUtils
import org.springframework.security.authentication.AccountExpiredException
import org.springframework.security.authentication.CredentialsExpiredException
import org.springframework.security.authentication.DisabledException
import org.springframework.security.authentication.LockedException
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.security.web.WebAttributes
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter
import org.springframework.web.servlet.ModelAndView
import proyecto.base.util.Captcha
import proyecto.base.util.CriptoUtils
import proyecto.base.util.EmailUtils

import javax.servlet.http.HttpServletResponse

class LoginController {

    def grailsApplication
    def authenticationTrustResolver
    def springSecurityService
    def loginService


    private EmailUtils emailUtils = new EmailUtils()
    private CriptoUtils criptoUtils = new CriptoUtils()

    PageRenderer groovyPageRenderer = new PageRenderer()

    /**
     * Default action; redirects to 'defaultTargetUrl' if logged in, /login/auth otherwise.
     */
    def index = {
        if (springSecurityService.isLoggedIn()) {
            redirect uri: SpringSecurityUtils.securityConfig.successHandler.defaultTargetUrl
        }
        else {
            redirect action: 'auth', params: params
        }
    }

    /**
     * Show the login page.
     */
    def auth = {
        def config = SpringSecurityUtils.securityConfig
        String sessionIp = request.getRemoteAddr()
        if (springSecurityService.isLoggedIn()) {
            redirect uri: config.successHandler.defaultTargetUrl
            return
        }

        loginService.setSessionIp(sessionIp) // asignamos la ip de session

        render view: '/inicio/index', model: [rememberMeParameter: config.rememberMe.parameter]
    }

    /**
     * The redirect action for Ajax requests.
     */
    def authAjax = {
        response.setHeader 'Location', SpringSecurityUtils.securityConfig.auth.ajaxLoginFormUrl
        response.sendError HttpServletResponse.SC_UNAUTHORIZED
    }

    /**
     * Show denied page.
     */
    def denied = {
        if (springSecurityService.isLoggedIn() &&
                authenticationTrustResolver.isRememberMe(SecurityContextHolder.context?.authentication)) {
            // have cookie but the page is guarded with IS_AUTHENTICATED_FULLY
            redirect action: 'full', params: params
        }
    }

    /**
     * Login page for users with a remember-me cookie but accessing a IS_AUTHENTICATED_FULLY page.
     */
    def full = {
        def config = SpringSecurityUtils.securityConfig
        render view: '/inicio/index', params: params,
                model: [hasCookie: authenticationTrustResolver.isRememberMe(SecurityContextHolder.context?.authentication),
                        postUrl: "${request.contextPath}${config.apf.filterProcessesUrl}"]
    }

    /**
     * Callback after a failed login. Redirects to the auth page with a warning message.
     */
    def authfail = {

        def correoElectronico = session[UsernamePasswordAuthenticationFilter.SPRING_SECURITY_LAST_USERNAME_KEY]
        String msg = ''
        def exception = session[WebAttributes.AUTHENTICATION_EXCEPTION]
        if (exception) {
            if (exception instanceof AccountExpiredException) {
                msg = g.message(code: "springSecurity.errors.login.expired")
            }
            else if (exception instanceof CredentialsExpiredException) {
                msg = g.message(code: "springSecurity.errors.login.passwordExpired")
            }
            else if (exception instanceof DisabledException) {
                msg = g.message(code: "springSecurity.errors.login.disabled")
            }
            else if (exception instanceof LockedException) {
                msg = g.message(code: "springSecurity.errors.login.locked")
            }
            else {
                msg = g.message(code: "springSecurity.errors.login.fail")
            }
        }

        if (springSecurityService.isAjax(request)) {
            render([error: msg] as JSON)
        }
        else {
            flash.message = msg
            redirect action: 'auth', params: params
        }
    }

    /**
     * The Ajax success redirect url.
     */
    def ajaxSuccess = {
        session.usuario = springSecurityService.currentUser
        render([success: true, username: springSecurityService.authentication.name] as JSON)
    }

    /**
     * The Ajax denied redirect url.
     */
    def ajaxDenied = {
        render([error: 'access denied'] as JSON)
    }

    def errorAcceso = {
        def model = [:]
        return new ModelAndView("/login/Recuperar", model)
    }

    def registro = {
        log.info("Ingresando a la pantalla de registro")
        this.generateCaptcha()
        [captcha: session.captcha, usuarioSistema: new UsuarioSistema()]
    }

    def instruccionesToolTip = {
        render """
		Por favor teclee un correo electr\u00f3nico v\u00e1lido tomando en cuenta las siguientes reglas:
		<br>--Debe ser un correo electr\u00f3nico valido(este correo sera utilizado para la funcionalidad de recuperaci\u00f3n de contrase\u00f1a).
		<br>--Debe tener al menos 5 caracteres.
		<br>Los campos marcados con * son obligatorios.
		"""
    }

    def save = {
        def password = this.criptoUtils.getNuevaContrasenia()

        def usuarioSistema = new UsuarioSistema()
        usuarioSistema.properties = params

        usuarioSistema.password = password
        usuarioSistema.enabled = false
        usuarioSistema.accountExpired = false
        usuarioSistema.accountLocked = false
        usuarioSistema.passwordExpired = false



        def model = [:]

        // Validando errores generales
        def usuarioSistemaDuplicado = UsuarioSistema.findByUsernameAndEnabled(usuarioSistema?.username, true)
        usuarioSistema.validate()

        if (usuarioSistema.hasErrors() ) {
            log.info(usuarioSistema.errors)

            model += [usuarioSistema: usuarioSistema]
        } else {
            if (!params.captcha) {
                flash.message = "${message(code: 'registgro.error.captcha', default: 'Codigo de verificaci\u00f3n inv\u00e1lido')}"
                model += [usuarioSistema: usuarioSistema]
            } else {
                if (usuarioSistemaDuplicado == null && params.captcha == session.captcha) {
                    try {
                        log.info("Enviando notificacion a el usuario: ${usuarioSistema.username}")

                        this.emailUtils.subject = grailsApplication.config.mail.default.subject.register
                        this.emailUtils.from = grailsApplication.config.mail.default.no.reply //"no-reply@ran.gob.mx"
                        this.emailUtils.contentHtml = groovyPageRenderer.render(template: '/email/registroUsuario',
                                model: [username: usuarioSistema.nombre]) //this.getHtmlRegistro(usuarioSistema)
                        log.debug("Cuerpo del mensaje "+this.emailUtils.contentHtml)
                        this.emailUtils.recipients = ["${usuarioSistema.username}"]
                        this.emailUtils.sendEmail();

                        // Hasta aquí
                        log.info("Guardando el usuario: ${usuarioSistema.username}")
                        usuarioSistema.save(flush: true)
                        session.captcha == ""
                        return new ModelAndView("/login/RegistroExitoso", model)
                    } catch (Exception e) {
                        log.error "Error - "+e.message
                        flash.error = "${message(code: 'error.conexion.internet', default: 'Verifique su conexi\u00f3n a internet.')}"
                        model += [usuarioSistema: usuarioSistema]
                    }
                } else {
                    model += [usuarioSistema:usuarioSistema]

                    if (params.captcha == session.captcha) {
                        flash.error = "${message(code: 'directorio.error.email', default: 'Error', args: [usuarioSistema.username])}"
                    } else {
                        flash.error = "${message(code: 'registgro.error.captcha', default: 'Codigo de verificaci\u00f3n invalido')}"
                        this.generateCaptcha()
                    }
                    this.generateCaptcha()
                }
            }
        }

        return new ModelAndView("/login/Registro", model)
    }

    def recover = {
        def empleado
        def model = [:]

        if (params.username) {
            String password = this.criptoUtils.getNuevaContrasenia()
            def usuario = UsuarioSistema.findByUsername(params.username)

            if (usuario) {
                if (usuario.enabled) {
                    try {

                        usuario.password = password
                        usuario.save(flush: true)
                        log.info("Enviando notificacion a el usuario: ${params.username}")
                        this.emailUtils.subject = grailsApplication.config.mail.default.subject.recovery
                        this.emailUtils.from = grailsApplication.config.mail.default.no.reply // "no-reply@ran.gob.mx"
                        this.emailUtils.contentHtml = groovyPageRenderer.render(template:'/email/recuperacionContrasena',
                                model:[username:usuario.username, password:password]) //this.getHtmlRecovery(usuario, password)
                        this.emailUtils.recipients = ["${params.email}"]
                        this.emailUtils.sendEmail();
                        flash.success = 'La Recuperaci\u00F3n de la contrase\u00F1a ha sido exitoso, y la informaci\u00F3n ha sido enviada a su correo electr\u00F3nico registrado dentro del sistema.'
                        return new ModelAndView("/login/recuperarExitoso", model)
                    } catch (Exception e) {
                        request.warning = "${message(code: ' ', default: 'Verifique su conexión a internet.')}"
                    }
                } else {
                    request.message = 'Lo sentimos, no hemos podido recuperar su cuenta debido a que no esta dado de alta en el sistema.'
                }
            } else {
                request.warning = "${message(code: 'recupera.error.contrasenia', default: 'Ingrese una direcci&oacute;n de correo electr&oacute;nico valida')}"
            }
        } else {
            request.error = "${message(code: 'recupera.error.contrasenia', default: 'Ingrese una direcci&oacute;n de correo electr&oacute;nico')}"
        }
        return new ModelAndView("/login/recuperar", model)
    }

    def captcha = {
        render this.generateCaptcha()
    }

    def generateCaptcha() {
        session.captcha = new Captcha().CadenaAleatoria()
        session.captcha
    }

}
