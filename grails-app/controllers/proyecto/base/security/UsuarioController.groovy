package proyecto.base.security

import grails.converters.JSON

class UsuarioController {

    def springSecurityService

    def validaContrasenia(){
        log.debug("Validando contrase\u00f1as")
        def respuesta = [:]
        if(params.contraseniaNueva.equals(params.confirmarNueva)){
            log.debug("Contrase\u00f1as iguales $params")
            respuesta << [exito : message(code: 'contrasenia.concidente', default:'Las contrase\u00f1as coinciden.')]

        }else {
            log.debug("Contrase\u00f1as diferentes $params")
            respuesta << [error : message(code: 'contrasenia.no.concidente', default:'Las contrase\u00f1as no coinciden.')]

        }
        render respuesta as JSON
    }

    def actualizarContrasenia(){
        flash.clear()
        log.debug("Recibiendo parametros $params")
        def user = springSecurityService.getCurrentUser()
        def usuarioSistema = UsuarioSistema.get(user.id)
        def contraseniaActual = springSecurityService.encodePassword(params.contraseniaActual)
        if(usuarioSistema.password.equals(contraseniaActual)){
            usuarioSistema.password = params.contraseniaNueva
            if (!usuarioSistema.save()){
                flash.error = message(code: 'contrasenia.cambio.error', default:'La contrase\u00f1a no se pudo actualizar')
                log.debug(flash.error)
            }else {
                flash.message = message(code: 'contrasenia.cambio.success', default:'Su contrase\u00f1a ha sido actualizada')
                log.debug(flash.message)
            }
        }else{
            flash.error = message(code: 'contrasenia.cambio.error', default:'La contrase\u00f1a actual no coincide')
            log.debug(flash.error)
        }
        render template:'/common/mensajes'
    }

    def actualizarUsuario() {
        def usuario = UsuarioSistema.get(session.usuario.id)
        usuario.properties = params
        usuario.beforeUpdate()
        if(!usuario.save() && flash.error){
            flash.error = "El usuario no se ha podido actualizar"
        } else{
            flash.success = "El usuario ha sido actualizado correctamente"
        }

        redirect action: 'index', controller:'inicio'
    }


}
