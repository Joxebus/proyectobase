package proyecto.base.util

import proyecto.base.files.Archivo


class ManejoArchivosController {

    def manejoArchivosService

    def descargarArchivo() {
        File archivoLocal = manejoArchivosService.obtenerArchivo([idArchivo:params.id])
        def archivo = Archivo.get(params.id)
        response.setHeader("Content-disposition", "attachment;filename=${archivo.toString()}")
        response.contentType = archivo.contentType
        response.outputStream << archivoLocal.getBytes()
        response.outputStream.flush()
    }
}
