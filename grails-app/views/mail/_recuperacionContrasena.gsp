<!doctype html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Recuperar Contrase&ntilde;a</title>
    <style type="text/css">
    <!--
    body {
        font: Arial, "Lucida Grande", "Lucida Sans Unicode", Helvetica, Verdana, sans-serif
        margin-left: 50px;
        margin-right: 50px;
        background-attachment: fixed;
    }
    .style1 {font-size: 20}
    -->
    </style></head>
<body>
<div align="center">


    <table align="center" width="75%">
        <tr>
            <td>
                <p><strong>Estimado usuario: ${username}</strong></p>
            </td>
        </tr>
        <tr>
            <td>
                <p align="justify">La cuenta de usuario con la que usted fue registrado en el sistema
                    <strong><g:meta name="app.name"/></strong> hace referencia a los siguientes datos:</p>
            </td>
        </tr>
        <tr>
            <td>

                <strong>Usuario:</strong> ${username}<br/>
                <strong>Contrase&ntilde;a:</strong> ${password}<br/>

            </td>
        </tr>
        <tr>
            <td>
                <p align="justify">Le solicitamos que respalde en un lugar seguro su usuario y contrase&ntilde;a
                ya que en caso de olvido o p&eacute;rdida de contrase&ntilde;a, el sistema podr&aacute;
                    otorgarle una nueva contrase&ntilde;a ingresando el usuario con el que se registro;
                dicha contrase&ntilde;a ser&aacute; enviada al correo electr&oacute;nico <strong>${email}</strong>
                    que nos proporciono cuando realizo su registro.</p>
            </td>
        </tr>
        <tr>
            <td>
                <p>Gracias por su inter&eacute;s en utilizar nuestro sistema.</p>
            </td>
        </tr>
        <tr>
            <td>
                <p><strong>Atentamente</strong>
                    <br/>Administraci&oacute;n del sistema <g:meta name="app.name"/></p>
            </td>
        </tr>
    </table>
    <br/><br/>

</body>
</html>