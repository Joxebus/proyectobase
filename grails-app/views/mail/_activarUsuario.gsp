<!doctype html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Activaci&oacute;n de Cuenta</title>
    <style type="text/css">
    <!--
    body {
        font: Arial, "Lucida Grande", "Lucida Sans Unicode", Helvetica, Verdana, sans-serif
        margin-left: 50px;
        margin-right: 50px;
        background-attachment: fixed;
    }
    .style1 {font-size: 20}
    -->
    </style></head>
<body>
<div align="center">

    <table align="center" width="75%">
        <tr>
            <td><p align="justify"><strong>Estimado usuario: ${username}</strong></p></td>
        </tr>
        <tr>
            <td><p align="justify">Su cuenta de usuario ha sido activada en el sistema <strong><g:meta name="app.name"/></strong>
                . Los datos con los que usted podr&aacute; ingresar al sistema son los siguientes:</p></td>
        </tr>
        <tr>
            <td><p align="justify"><strong>Usuario:</strong> ${username}</strong></p>
                <p align="justify"><strong>Contrase&ntilde;a:</strong> ${password}</strong></p>
            </td>
        </tr>
        <tr>
            <td><p align="justify">Le solicitamos que respalde en un lugar seguro su usuario y contrase&ntilde;a
            ya que en caso de olvido o perdida de contrase&ntilde;a, el sistema podr&aacute; otorgarle una nueva
            contrase&ntilde;a ingresando el usuario con el que se registro; dicha contrase&ntilde;a ser&aacute;
                enviada al correo electr&oacute;nico <strong>${usuario.nombre}</strong> que nos proporcion&oacute;
                cuando realiz&oacute; su registro.</p></td>
        </tr>
        <tr>
            <td><p align="justify">Gracias por su interes en utilizar nuestro sistema.</p></td>
        </tr>
        <tr>
            <td><p align="justify"><strong>Atentamente</strong>
                <br/>Administraci&oacute;n del sistema <strong><g:meta name="app.name"/></strong></p>
            </td>
        </tr>
    </table>
    <br/><br/>

</body>
</html>