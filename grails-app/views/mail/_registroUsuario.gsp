<!doctype html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Registro de Usuario</title>
    <style type="text/css">
    <!--
    body {
        font: Arial, "Lucida Grande", "Lucida Sans Unicode", Helvetica, Verdana, sans-serif
        margin-left: 50px;
        margin-right: 50px;
        background-attachment: fixed;
    }
    .style1 {font-size: 20}
    -->
    </style></head>
<body>
<div align="center">
</div> <br/>

<table align="center" width="75%">
    <tr>
        <td>
            <strong>Estimado usuario: ${username}</strong>
        </td>
    </tr>
    <tr>
        <td>
            <p align="justify">Bienvenido a la <strong><g:meta name="app.name"/></strong>,
            por el momento su registro est&aacute; siendo aprobado por el administrador del sistema.</p>
        </td>
    </tr>
    <tr>
        <td>
            <p align="justify">Una vez que se haya aprobado dicha solicitud usted recibir&aacute; un usuario y una contrase&ntilde;a
            con la cual podr&aacute; acceder al sistema la pr&oacute;xima vez. En caso de que su solicitud no haya sido aprobada
            tambi&eacute;n recibir&aacute; un correo con las causas del porque no fue aprobada dicha solicitud.</p>
        </td>
    </tr>
    <tr>
        <td>
            <p>Le agradecemos su inter&eacute;s de registrarse en nuestro sistema.</p>
        </td>
    </tr>
    <tr>
        <td>
            <p><strong>Atentamente</strong><br/>Administraci&oacute;n sistema <strong><g:meta name="app.name"/></strong></p>
        </td>
    </tr>
</table>
<br/></div>

</body>
</html>