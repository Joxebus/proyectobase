
<%@ page import="proyecto.base.files.Archivo" %>
<!doctype html>
<html>
	<head>
		<meta name="layout" content="bootstrap_fluid">
		<g:set var="entityName" value="${message(code: 'archivo.label', default: 'Archivo')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body>

    <div class="row-fluid">
        <div class="span3">
            <div class="well sidebar-nav">
                <ul class="nav nav-list">
                    <li class="nav-header"><g:message code="default.actions.label" default="Acciones" /></li>
				<li><g:link class="list" action="list">
                    <i class="icon-th-list"></i><g:message code="default.list.label" args="[entityName]" /></g:link>
                </li>
				<li><g:link class="create" action="create">
                    <i class="icon-plus"></i><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
                </div>
		</div>
		<div id="show-archivo" class="span9" role="main">
			<h1><g:message code="default.show.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="alert alert-success" role="status">${flash.message}</div>
			</g:if>
			<ol class="property-list archivo">
			
				<g:if test="${archivoInstance?.tamano}">
				<li class="fieldcontain">
					<span id="tamano-label" class="property-label"><g:message code="archivo.tamano.label" default="Tamano" /></span>
					
						<span class="property-value" aria-labelledby="tamano-label"><g:fieldValue bean="${archivoInstance}" field="tamano"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${archivoInstance?.contentType}">
				<li class="fieldcontain">
					<span id="contentType-label" class="property-label"><g:message code="archivo.contentType.label" default="Content Type" /></span>
					
						<span class="property-value" aria-labelledby="contentType-label"><g:fieldValue bean="${archivoInstance}" field="contentType"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${archivoInstance?.extension}">
				<li class="fieldcontain">
					<span id="extension-label" class="property-label"><g:message code="archivo.extension.label" default="Extension" /></span>
					
						<span class="property-value" aria-labelledby="extension-label"><g:fieldValue bean="${archivoInstance}" field="extension"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${archivoInstance?.nombre}">
				<li class="fieldcontain">
					<span id="nombre-label" class="property-label"><g:message code="archivo.nombre.label" default="Nombre" /></span>
					
						<span class="property-value" aria-labelledby="nombre-label"><g:fieldValue bean="${archivoInstance}" field="nombre"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${archivoInstance?.ubicacionLocal}">
				<li class="fieldcontain">
					<span id="ubicacionLocal-label" class="property-label"><g:message code="archivo.ubicacionLocal.label" default="Ubicacion Local" /></span>
					
						<span class="property-value" aria-labelledby="ubicacionLocal-label"><g:fieldValue bean="${archivoInstance}" field="ubicacionLocal"/></span>
					
				</li>
				</g:if>
			
			</ol>
			<g:form>
				<fieldset class="buttons">
					<g:hiddenField name="id" value="${archivoInstance?.id}" />
					<g:link class="btn btn-primary" action="edit" id="${archivoInstance?.id}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
					<g:actionSubmit class="btn" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
				</fieldset>
			</g:form>
		</div>
        </div>
	</body>
</html>
