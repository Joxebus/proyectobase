<%@ page import="proyecto.base.files.Archivo" %>



<div class="fieldcontain ${hasErrors(bean: archivoInstance, field: 'tamano', 'error')} required">
	<label for="tamano">
		<g:message code="archivo.tamano.label" default="Tamano" />
		<span class="required-indicator">*</span>
	</label>
	<g:field name="tamano" placeholder="tamano" class="input-large" type="number" max="5242880" value="${archivoInstance.tamano}" required=""/>
</div>

<div class="fieldcontain ${hasErrors(bean: archivoInstance, field: 'contentType', 'error')} ">
	<label for="contentType">
		<g:message code="archivo.contentType.label" default="Content Type" />
		
	</label>
	<g:textField  name="contentType" placeholder="contentType" class="input-large" value="${archivoInstance?.contentType}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: archivoInstance, field: 'extension', 'error')} ">
	<label for="extension">
		<g:message code="archivo.extension.label" default="Extension" />
		
	</label>
	<g:textField  name="extension" placeholder="extension" class="input-large" value="${archivoInstance?.extension}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: archivoInstance, field: 'nombre', 'error')} ">
	<label for="nombre">
		<g:message code="archivo.nombre.label" default="Nombre" />
		
	</label>
	<g:textField  name="nombre" placeholder="nombre" class="input-large" value="${archivoInstance?.nombre}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: archivoInstance, field: 'ubicacionLocal', 'error')} ">
	<label for="ubicacionLocal">
		<g:message code="archivo.ubicacionLocal.label" default="Ubicacion Local" />
		
	</label>
	<g:textField  name="ubicacionLocal" placeholder="ubicacionLocal" class="input-large" value="${archivoInstance?.ubicacionLocal}"/>
</div>

