<%@ page import="proyecto.base.files.Archivo" %>
<!doctype html>
<html>
	<head>
		<meta name="layout" content="bootstrap_fluid">
		<g:set var="entityName" value="${message(code: 'archivo.label', default: 'Archivo')}" />
		<title><g:message code="default.edit.label" args="[entityName]" /></title>
	</head>
	<body>

    <div class="row-fluid">
        <div class="span3">
            <div class="well sidebar-nav">
                <ul class="nav nav-list">
                    <li class="nav-header"><g:message code="default.actions.label" default="Acciones" /></li>
                    <li><g:link action="list">
                        <i class="icon-th-list"></i><g:message code="default.list.label" args="[entityName]" /></g:link></li>
                    <li><g:link action="create">
                        <i class="icon-plus"></i><g:message code="default.new.label" args="[entityName]" /></g:link></li>
                </ul>
            </div>
		</div>
		<div id="edit-archivo" class="span9" role="main">
			<h1><g:message code="default.edit.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="alert alert-success" role="status">${flash.message}</div>
			</g:if>
			<g:hasErrors bean="${archivoInstance}">
                <div class="alert alert-error">
                    <ul class="unstyled" role="alert">
                        <g:eachError bean="${archivoInstance}" var="error">
                            <li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
                        </g:eachError>
                    </ul>
                </div>
			</g:hasErrors>
			<g:form method="post" >
				<g:hiddenField name="id" value="${archivoInstance?.id}" />
				<g:hiddenField name="version" value="${archivoInstance?.version}" />
				<fieldset class="form">
					<g:render template="form"/>
				</fieldset>
				<fieldset class="buttons">
					<g:actionSubmit class="btn btn-primary" action="update" value="${message(code: 'default.button.update.label', default: 'Update')}" />
					<g:actionSubmit class="btn" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" formnovalidate="" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
				</fieldset>
			</g:form>
		</div>
        </div>
	</body>
</html>
