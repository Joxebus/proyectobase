
<%@ page import="proyecto.base.files.Archivo" %>
<!doctype html>
<html>
	<head>
		<meta name="layout" content="bootstrap_fluid">
		<g:set var="entityName" value="${message(code: 'archivo.label', default: 'Archivo')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<div class="row-fluid">
            <div class="span3">
                <div class="well sidebar-nav">
                    <ul class="nav nav-list">
                        <li class="nav-header"><g:message code="default.actions.label" default="Acciones" /></li>
                        <li><g:link class="create" action="create">
                            <i class="icon-plus"></i><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			        </ul>
                </div>
            </div>


		<div id="list-archivo" class="span9" role="main">
			<h1><g:message code="default.list.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="alert alert-success" role="status">${flash.message}</div>
			</g:if>
			<table class="table table-hover">
				<thead>
					<tr>
					
						<g:sortableColumn property="tamano" title="${message(code: 'archivo.tamano.label', default: 'Tamano')}" />
					
						<g:sortableColumn property="contentType" title="${message(code: 'archivo.contentType.label', default: 'Content Type')}" />
					
						<g:sortableColumn property="extension" title="${message(code: 'archivo.extension.label', default: 'Extension')}" />
					
						<g:sortableColumn property="nombre" title="${message(code: 'archivo.nombre.label', default: 'Nombre')}" />
					
						<g:sortableColumn property="ubicacionLocal" title="${message(code: 'archivo.ubicacionLocal.label', default: 'Ubicacion Local')}" />
					
                        <th class="sortable" width="10px">Acciones</th>
					</tr>
				</thead>
				<tbody>
				<g:each in="${archivoInstanceList}" status="i" var="archivoInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
					
						<td><g:link action="show" id="${archivoInstance.id}">${fieldValue(bean: archivoInstance, field: "tamano")}</g:link></td>
					
						<td>${fieldValue(bean: archivoInstance, field: "contentType")}</td>
					
						<td>${fieldValue(bean: archivoInstance, field: "extension")}</td>
					
						<td>${fieldValue(bean: archivoInstance, field: "nombre")}</td>
					
						<td>${fieldValue(bean: archivoInstance, field: "ubicacionLocal")}</td>
					
                        <td><!-- Icons -->
                            <g:form>
                                <g:hiddenField name="id" value="${archivoInstance.id}"/>
                                <g:actionSubmitImage class="table-button" value="${message(code: 'global.edit.label', default: 'Editar')}" src="${resource(dir: 'images/icons', file: 'pencil.png')}" action="edit" formnovalidate=""/>
                                <g:actionSubmitImage class="table-button" value="${message(code: 'global.delete.label', default: 'Eliminar')}" src="${resource(dir: 'images/icons', file: 'cross.png')}" action="delete" formnovalidate="" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');"/>
                            </g:form>
                        </td>
					</tr>
				</g:each>
				</tbody>
			</table>


			<g:paginate total="${archivoInstanceTotal}" />

		</div>
        </div>
	</body>
</html>
