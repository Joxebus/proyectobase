<script type="text/javascript">
    $(document).ready(function(){
        $.ajax({
            type: "POST",
            url: "${createLink(action: 'getAllClients', controller:'util')}",
            success : function(response) {
                $("#nombreCliente").autocomplete({
                    source: response
                });
            }
        });
    })
</script>