
<%@ page import="proyecto.base.security.UsuarioSistema" %>
<!doctype html>
<html>
	<head>
		<meta name="layout" content="bootstrap_fluid">
		<g:set var="entityName" value="${message(code: 'usuarioSistema.label', default: 'UsuarioSistema')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<div class="row-fluid">
            <div class="span3">
                <div class="well sidebar-nav">
                    <ul class="nav nav-list">
                        <li class="nav-header"><g:message code="default.actions.label" default="Acciones" /></li>
                        <li><g:link class="create" action="create">
                            <i class="icon-plus"></i><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			        </ul>
                </div>
            </div>


		<div id="list-usuarioSistema" class="span9" role="main">
			<h1><g:message code="default.list.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="alert alert-success" role="status">${flash.message}</div>
			</g:if>
			<table class="table table-hover">
				<thead>
					<tr>
					
						<g:sortableColumn property="username" title="${message(code: 'usuarioSistema.username.label', default: 'Username')}" />
					
						<g:sortableColumn property="password" title="${message(code: 'usuarioSistema.password.label', default: 'Password')}" />
					
						<g:sortableColumn property="tipoUsuario" title="${message(code: 'usuarioSistema.tipoUsuario.label', default: 'Tipo Usuario')}" />
					
						<g:sortableColumn property="nombre" title="${message(code: 'usuarioSistema.nombre.label', default: 'Nombre')}" />
					
						<g:sortableColumn property="apellidoPaterno" title="${message(code: 'usuarioSistema.apellidoPaterno.label', default: 'Apellido Paterno')}" />
					
						<g:sortableColumn property="apellidoMaterno" title="${message(code: 'usuarioSistema.apellidoMaterno.label', default: 'Apellido Materno')}" />
					
                        <th class="sortable" width="10px">Acciones</th>
					</tr>
				</thead>
				<tbody>
				<g:each in="${usuarioSistemaInstanceList}" status="i" var="usuarioSistemaInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
					
						<td><g:link action="show" id="${usuarioSistemaInstance.id}">${fieldValue(bean: usuarioSistemaInstance, field: "username")}</g:link></td>
					
						<td>${fieldValue(bean: usuarioSistemaInstance, field: "password")}</td>
					
						<td>${fieldValue(bean: usuarioSistemaInstance, field: "tipoUsuario")}</td>
					
						<td>${fieldValue(bean: usuarioSistemaInstance, field: "nombre")}</td>
					
						<td>${fieldValue(bean: usuarioSistemaInstance, field: "apellidoPaterno")}</td>
					
						<td>${fieldValue(bean: usuarioSistemaInstance, field: "apellidoMaterno")}</td>
					
                        <td><!-- Icons -->
                            <g:form>
                                <g:hiddenField name="id" value="${usuarioSistemaInstance.id}"/>
                                <g:actionSubmitImage class="table-button" value="${message(code: 'global.edit.label', default: 'Editar')}" src="${resource(dir: 'images/icons', file: 'pencil.png')}" action="edit" formnovalidate=""/>
                                <g:actionSubmitImage class="table-button" value="${message(code: 'global.delete.label', default: 'Eliminar')}" src="${resource(dir: 'images/icons', file: 'cross.png')}" action="delete" formnovalidate="" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');"/>
                            </g:form>
                        </td>
					</tr>
				</g:each>
				</tbody>
			</table>

			<g:paginate total="${usuarioSistemaInstanceTotal}" />

		</div>
        </div>
	</body>
</html>
