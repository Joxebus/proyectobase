<script type="text/javascript">
    $(document).ready(function () {

        $('#spinner').bind("ajaxSend", function() {
            $(this).show();
            $('#content-modal').hide();
        }).bind("ajaxComplete", function() {
            $(this).hide();
            $('#content-modal').show();
        });

    });

    $('[data-load-remote]').on('click',function(e) {
        e.preventDefault();
        var $this = $(this);
        var remote = $this.data('load-remote');
        if(remote) {
            $($this.data('remote-target')).load(remote);
        }
    });
</script>

<div id="modalActivarUsuario" class="modal hide fade" tabindex="-1" role="dialog">
    <div class="modal-header">
        <a class="close" data-dismiss="modal">×</a>
        <h3>Asignar rol</h3>
    </div>
    <g:form class="form-horizontal" controller="solicitudCuenta">
    <g:render template="/common/loading"/>
    <div id="content-modal" class="modal-body">

    </div>
    <div class="modal-footer">
        <a href="#" class="btn" data-dismiss="modal" aria-hidden="true">Close</a>
        <g:actionSubmit value="Guardar" class="btn btn-primary" action="asignarRol"/>

    </div>
    </g:form>
</div>

