<%@ page import="proyecto.base.security.UsuarioSistema" %>



<div class="fieldcontain ${hasErrors(bean: usuarioSistemaInstance, field: 'username', 'error')} required">
    <div class="control-group">
        <label class="control-label" for="username">
		<g:message code="usuarioSistema.username.label" default="Username" />
		<span class="required-indicator">*</span>
	</label>
        <div class="controls">
	        <g:field type="email" name="username" placeholder="username" class="input-large" maxlength="60" required="" value="${usuarioSistemaInstance?.username}"/>
        </div>
    </div>
</div>


<div class="fieldcontain ${hasErrors(bean: usuarioSistemaInstance, field: 'tipoUsuario', 'error')} ">
    <div class="control-group">
        <label class="control-label" for="tipoUsuario">
		<g:message code="usuarioSistema.tipoUsuario.label" default="Tipo Usuario" />
		
	</label>
        <div class="controls">
	        <g:textField  name="tipoUsuario" placeholder="tipoUsuario" class="input-large" value="${usuarioSistemaInstance?.tipoUsuario}"/>
        </div>
    </div>
</div>

<div class="fieldcontain ${hasErrors(bean: usuarioSistemaInstance, field: 'nombre', 'error')} required">
    <div class="control-group">
        <label class="control-label" for="nombre">
		<g:message code="usuarioSistema.nombre.label" default="Nombre" />
		<span class="required-indicator">*</span>
	</label>
        <div class="controls">
	        <g:textField  name="nombre" placeholder="nombre" class="input-large" maxlength="60" required="" value="${usuarioSistemaInstance?.nombre}"/>
        </div>
    </div>
</div>

<div class="fieldcontain ${hasErrors(bean: usuarioSistemaInstance, field: 'apellidoPaterno', 'error')} required">
    <div class="control-group">
        <label class="control-label" for="apellidoPaterno">
		<g:message code="usuarioSistema.apellidoPaterno.label" default="Apellido Paterno" />
		<span class="required-indicator">*</span>
	</label>
        <div class="controls">
	        <g:textField  name="apellidoPaterno" placeholder="apellidoPaterno" class="input-large" maxlength="60" required="" value="${usuarioSistemaInstance?.apellidoPaterno}"/>
        </div>
    </div>
</div>

<div class="fieldcontain ${hasErrors(bean: usuarioSistemaInstance, field: 'apellidoMaterno', 'error')} required">
    <div class="control-group">
        <label class="control-label" for="apellidoMaterno">
		<g:message code="usuarioSistema.apellidoMaterno.label" default="Apellido Materno" />
		<span class="required-indicator">*</span>
	</label>
        <div class="controls">
	        <g:textField  name="apellidoMaterno" placeholder="apellidoMaterno" class="input-large" maxlength="60" required="" value="${usuarioSistemaInstance?.apellidoMaterno}"/>
        </div>
    </div>
</div>

<div class="fieldcontain ${hasErrors(bean: usuarioSistemaInstance, field: 'enabled', 'error')} ">
    <div class="control-group">
        <label class="control-label" for="enabled">
		<g:message code="usuarioSistema.enabled.label" default="Enabled" />
		
	</label>
        <div class="controls">
	        <g:checkBox name="enabled" value="${usuarioSistemaInstance?.enabled}" />
        </div>
    </div>
</div>

