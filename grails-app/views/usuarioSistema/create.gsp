<%@ page import="proyecto.base.security.UsuarioSistema" %>
<!doctype html>
<html>
	<head>
		<meta name="layout" content="bootstrap_fluid">
		<g:set var="entityName" value="${message(code: 'usuarioSistema.label', default: 'UsuarioSistema')}" />
		<title><g:message code="default.create.label" args="[entityName]" /></title>
	</head>
	<body>

    <div class="row-fluid">
        <div class="span3">
            <div class="well sidebar-nav">
                <ul class="nav nav-list">
                    <li class="nav-header"><g:message code="default.actions.label" default="Acciones" /></li>
				    <li>
                        <g:link action="list">
                            <i class="icon-th-list"></i><g:message code="default.list.label" args="[entityName]" />
                        </g:link>
                    </li>
			    </ul>
            </div>
		</div>

		<div id="create-usuarioSistema" class="span9" role="main">
			<h1><g:message code="default.create.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			    <div class="alert alert-success" role="status">${flash.message}</div>
			</g:if>
			<g:hasErrors bean="${usuarioSistemaInstance}">
                <div class="alert alert-error">
                    <ul class="unstyled" role="alert">
                        <g:eachError bean="${usuarioSistemaInstance}" var="error">
                        <li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
                        </g:eachError>
                    </ul>
                </div>
			</g:hasErrors>
			<g:form class="form-horizontal" action="save" >
				<fieldset class="form">
					<g:render template="form"/>
				</fieldset>
				<fieldset class="buttons">
					<g:submitButton name="create" class="btn btn-primary" value="${message(code: 'default.button.create.label', default: 'Create')}" />
				</fieldset>
			</g:form>
		</div>
    </div>
	</body>
</html>
