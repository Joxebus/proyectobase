<%@ page import="proyecto.base.security.UsuarioSistema" %>
<%@ page import="proyecto.base.security.Permiso" %>

<g:hiddenField name="id" value="${usuarioSistemaInstance.id}"/>

<div class="fieldcontain ${hasErrors(bean: usuarioSistemaInstance, field: 'username', 'error')} required">
    <div class="control-group">
        <label class="control-label" for="username">
            <g:message code="usuarioSistema.username.label" default="Username" />

        </label>
        <div class="controls">
            ${fieldValue(bean: usuarioSistemaInstance, field: "username")}
        </div>
    </div>
</div>


<div class="fieldcontain ${hasErrors(bean: usuarioSistemaInstance, field: 'nombre', 'error')} required">
    <div class="control-group">
        <label class="control-label" for="nombre">
            <g:message code="usuarioSistema.nombre.label" default="Nombre" />

        </label>
        <div class="controls">
            ${usuarioSistemaInstance}
        </div>
    </div>
</div>

<div class="required">
    <div class="control-group">
        <label class="control-label" for="rol">
            <g:message code="usuarioSistema.rol.label" default="Rol" />
            <span class="required-indicator">*</span>
        </label>
        <div class="controls">
            <g:select name="rol" from="${Permiso.list()}"
                optionKey="id"
                noSelection="['':'-- Seleccione rol --']" />
        </div>
    </div>
</div>