
<%@ page import="proyecto.base.security.UsuarioSistema" %>
<!doctype html>
<html>
	<head>
		<meta name="layout" content="bootstrap_fluid">
		<g:set var="entityName" value="${message(code: 'usuarioSistema.label', default: 'UsuarioSistema')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body>

    <div class="row-fluid">
        <div class="span3">
            <div class="well sidebar-nav">
                <ul class="nav nav-list">
                    <li class="nav-header"><g:message code="default.actions.label" default="Acciones" /></li>
				<li><g:link class="list" action="list">
                    <i class="icon-th-list"></i><g:message code="default.list.label" args="[entityName]" /></g:link>
                </li>
				<li><g:link class="create" action="create">
                    <i class="icon-plus"></i><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
                </div>
		</div>
		<div id="show-usuarioSistema" class="span9" role="main">
			<h1><g:message code="default.show.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="alert alert-success" role="status">${flash.message}</div>
			</g:if>
			<ol class="property-list usuarioSistema">
			
				<g:if test="${usuarioSistemaInstance?.username}">
				<li class="fieldcontain">
					<span id="username-label" class="property-label"><g:message code="usuarioSistema.username.label" default="Username" /></span>
					
						<span class="property-value" aria-labelledby="username-label"><g:fieldValue bean="${usuarioSistemaInstance}" field="username"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${usuarioSistemaInstance?.password}">
				<li class="fieldcontain">
					<span id="password-label" class="property-label"><g:message code="usuarioSistema.password.label" default="Password" /></span>
					
						<span class="property-value" aria-labelledby="password-label"><g:fieldValue bean="${usuarioSistemaInstance}" field="password"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${usuarioSistemaInstance?.tipoUsuario}">
				<li class="fieldcontain">
					<span id="tipoUsuario-label" class="property-label"><g:message code="usuarioSistema.tipoUsuario.label" default="Tipo Usuario" /></span>
					
						<span class="property-value" aria-labelledby="tipoUsuario-label"><g:fieldValue bean="${usuarioSistemaInstance}" field="tipoUsuario"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${usuarioSistemaInstance?.nombre}">
				<li class="fieldcontain">
					<span id="nombre-label" class="property-label"><g:message code="usuarioSistema.nombre.label" default="Nombre" /></span>
					
						<span class="property-value" aria-labelledby="nombre-label"><g:fieldValue bean="${usuarioSistemaInstance}" field="nombre"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${usuarioSistemaInstance?.apellidoPaterno}">
				<li class="fieldcontain">
					<span id="apellidoPaterno-label" class="property-label"><g:message code="usuarioSistema.apellidoPaterno.label" default="Apellido Paterno" /></span>
					
						<span class="property-value" aria-labelledby="apellidoPaterno-label"><g:fieldValue bean="${usuarioSistemaInstance}" field="apellidoPaterno"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${usuarioSistemaInstance?.apellidoMaterno}">
				<li class="fieldcontain">
					<span id="apellidoMaterno-label" class="property-label"><g:message code="usuarioSistema.apellidoMaterno.label" default="Apellido Materno" /></span>
					
						<span class="property-value" aria-labelledby="apellidoMaterno-label"><g:fieldValue bean="${usuarioSistemaInstance}" field="apellidoMaterno"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${usuarioSistemaInstance?.enabled}">
				<li class="fieldcontain">
					<span id="enabled-label" class="property-label"><g:message code="usuarioSistema.enabled.label" default="Enabled" /></span>
					
						<span class="property-value" aria-labelledby="enabled-label"><g:formatBoolean boolean="${usuarioSistemaInstance?.enabled}" /></span>
					
				</li>
				</g:if>
			
				<g:if test="${usuarioSistemaInstance?.usuarioModifica}">
				<li class="fieldcontain">
					<span id="usuarioModifica-label" class="property-label"><g:message code="usuarioSistema.usuarioModifica.label" default="Usuario Modifica" /></span>
					
						<span class="property-value" aria-labelledby="usuarioModifica-label"><g:fieldValue bean="${usuarioSistemaInstance}" field="usuarioModifica"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${usuarioSistemaInstance?.estadoLogico}">
				<li class="fieldcontain">
					<span id="estadoLogico-label" class="property-label"><g:message code="usuarioSistema.estadoLogico.label" default="Estado Logico" /></span>
					
						<span class="property-value" aria-labelledby="estadoLogico-label"><g:fieldValue bean="${usuarioSistemaInstance}" field="estadoLogico"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${usuarioSistemaInstance?.captcha}">
				<li class="fieldcontain">
					<span id="captcha-label" class="property-label"><g:message code="usuarioSistema.captcha.label" default="Captcha" /></span>
					
						<span class="property-value" aria-labelledby="captcha-label"><g:fieldValue bean="${usuarioSistemaInstance}" field="captcha"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${usuarioSistemaInstance?.accountExpired}">
				<li class="fieldcontain">
					<span id="accountExpired-label" class="property-label"><g:message code="usuarioSistema.accountExpired.label" default="Account Expired" /></span>
					
						<span class="property-value" aria-labelledby="accountExpired-label"><g:formatBoolean boolean="${usuarioSistemaInstance?.accountExpired}" /></span>
					
				</li>
				</g:if>
			
				<g:if test="${usuarioSistemaInstance?.accountLocked}">
				<li class="fieldcontain">
					<span id="accountLocked-label" class="property-label"><g:message code="usuarioSistema.accountLocked.label" default="Account Locked" /></span>
					
						<span class="property-value" aria-labelledby="accountLocked-label"><g:formatBoolean boolean="${usuarioSistemaInstance?.accountLocked}" /></span>
					
				</li>
				</g:if>
			
				<g:if test="${usuarioSistemaInstance?.passwordExpired}">
				<li class="fieldcontain">
					<span id="passwordExpired-label" class="property-label"><g:message code="usuarioSistema.passwordExpired.label" default="Password Expired" /></span>
					
						<span class="property-value" aria-labelledby="passwordExpired-label"><g:formatBoolean boolean="${usuarioSistemaInstance?.passwordExpired}" /></span>
					
				</li>
				</g:if>
			
			</ol>
			<g:form>
				<fieldset class="buttons">
					<g:hiddenField name="id" value="${usuarioSistemaInstance?.id}" />
					<g:link class="btn btn-primary" action="edit" id="${usuarioSistemaInstance?.id}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
					<g:actionSubmit class="btn" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
				</fieldset>
			</g:form>
		</div>
        </div>
	</body>
</html>
