<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="bootstrap_fluid"/>
    <style type="text/css">
        .captcha {


            padding-top: 20px;

            -moz-user-select: none;
            -khtml-user-select: none;
            -webkit-user-select: none;

            /*
              Introduced in IE 10.
              See http://ie.microsoft.com/testdrive/HTML5/msUserSelect/
            */
            -ms-user-select: none;
            user-select: none;
            font-size: 50px;
            font-family:serif;

        }
    </style>
    <title><g:message code="titulo.registro.usuario" default="Registro de usuario"/></title>
</head>

<body>
<div class="row-fluid">

    <div class="span4">
        <g:render template="/common/mensajes"/>
        <form method="post" class="form-horizontal">
            <fieldset>
                <legend>Formulario de registro</legend>

                <div class="control-group">
                    <label class="control-label">Nombre</label>

                    <div class="controls">
                        <g:textField name="nombre" required="" placeholder="nombre" value="${usuarioSistema.nombre}"/>
                    </div>
                </div>

                <div class="control-group">
                    <label class="control-label">Apellido Paterno</label>

                    <div class="controls">
                        <g:textField name="apellidoPaterno" required="" placeholder="apellido paterno"
                                     value="${usuarioSistema.apellidoPaterno}"/>
                    </div>
                </div>

                <div class="control-group">
                    <label class="control-label">Apellido Materno</label>

                    <div class="controls">
                        <g:textField name="apellidoMaterno" required="" placeholder="apllido materno"
                                     value="${usuarioSistema.apellidoMaterno}"/>
                    </div>
                </div>

                <div class="control-group">
                    <label class="control-label">Correo Electronico</label>

                    <div class="controls">
                        <g:textField name="username" required="" placeholder="correo electronico"/>
                    </div>
                </div>

                <div class="control-group">
                    <label class="control-label">
                        Introduzca los datos de la imagen:</label>

                    <div class="controls">
                        <g:textField class="text-input" name="captcha" required="" value="" placeholder="captcha"/>

                        <p class="captcha">
                            <%=session.captcha%>
                        </p>
                    </div>
                </div>


                <div class="control-group">
                    <div class="controls">
                        <g:actionSubmit value="Aceptar" class="btn btn-primary" action="save"/>

                        <a class="btn" href="${createLink(uri: '/')}">Cancelar</a>
                    </div>
                </div>

            </fieldset>
        </form>
    </div>
</div>
</body>
</html>