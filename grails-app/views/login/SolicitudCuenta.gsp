<%@ page import="proyecto.base.security.Permiso; proyecto.base.security.UsuarioSistemaPermiso;" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name="layout" content="bootstrap_fluid"/>
    <title><g:message code="solicitudes.title" default="Solicitudes de cuentas"/></title>
</head>


<body>
<h2>Solicitud de Cuenta Existente</h2>

<p id="page-intro">En &eacute;sta secci&oacute;n puede tener el control sobre las cuentas de los usuarios del sistema</p>


<div class="content-box" style="display: inline-block;"><!-- Start Content Box -->

    <div class="content-box-header">

        <h3>Listado de cuentas registradas</h3>

        <div class="clear">

        </div>

    </div> <!-- End .content-box-header -->

    <div class="content-box-content">

        <g:if test="${flash.success}">
            <div class="notification success png_bg">
                <a href="#" class="close"><img src="${resource(dir: 'images/icons', file: 'cross_grey_small.png')}" title="Cerrar notificaci&oacute;n" alt="cerrar"/></a>
                <div>
                    ${flash.success}
                </div>
            </div>
        </g:if>

        <g:if test="${flash.warning}">
            <div class="notification error png_bg">
                <a href="#" class="close"><img src="${resource(dir: 'images/icons', file: 'cross_grey_small.png')}" title="Cerrar notificaci&oacute;n" alt="cerrar"/></a>
                <div>
                    ${flash.warning}
                </div>
            </div>
        </g:if>

        <g:if test="${flash.message}">
            <div class="notification information png_bg">
                <a href="#" class="close"><img src="${resource(dir: 'images/icons', file: 'cross_grey_small.png')}" title="Cerrar notificaci&oacute;n" alt="cerrar"/></a>
                <div>
                    ${flash.message}
                </div>
            </div>
        </g:if>


        <g:hasErrors bean="${usuarioSistema}">
            <div class="notification error png_bg">
                <a href="#" class="close"><img src="${resource(dir: 'images/icons', file: 'cross_grey_small.png')}" title="Close this notification" alt="close" /></a>
                <div>
                    <g:eachError bean="${usuarioSistema}" var="error">
                        <li>
                            <g:message error="${error}"/>
                        </li>
                    </g:eachError>
                </div>
            </div>
        </g:hasErrors>

        <!--<fieldset>
            <g:form>
                <g:message code="Filtrar por:"/>
                <g:select name="parametroBuscar"
                          from="${['Nombre']}"
                          value="${value}"
                          keys="['1']"
                          required=""
                          noSelection="['':'Seleccione una opci\u00f3n']"/>
                <g:textField name="buscar" class="text-input" required=""/>
                <g:actionSubmit class="button" action="buscar" value="Buscar"/>
            </g:form>
        </fieldset> -->


        <table>
            <thead>
            <tr>

                <th>
                    <center>
                        <g:message code="solicitud.table.head.nombre" default="Nombre"/>
                    </center>
                </th>


                <th>
                    <g:message code="solicitud.table.head.email" default="Correo Electr&oacute;nico"/>
                </th>

                <th>
                    <center>
                        <g:message code="solicitud.table.head.rol" default="Rol"/>
                    </center>
                </th>

                <th>
                    <g:message code="solicitud.table.head.verificacion" default="Verificaci&oacute;n"/>
                </th>

                <th>
                    <g:message code="solicitud.table.head.cuenta" default="Cuenta"/>
                </th>

                <th>
                    <g:message code="solicitud.table.head.editar" default="Editar"/>
                </th>

                <th>
                    <g:message code="solicitud.table.head.eliminar" default="Eliminar"/>
                </th>

            </tr>
            </thead>
            <tbody>
            <g:each in="${usuariosList}" status="i" var="usuario">
                <tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
                <td>
                    ${usuario?.nombre}
                </td>


                <td>
                    ${usuario?.username}
                </td>
                <td>
                    <g:if test="${UsuarioSistemaPermiso.getByID(usuario.usuarioSistema.id) != null}">
                        <center>
                            <g:if test="${UsuarioSistemaPermiso.getByID(usuario.usuarioSistema.id).permiso.authority == 'ROLE_ADMIN'}">
                                ADMINISTRADOR
                            </g:if>

                            <g:if test="${UsuarioSistemaPermiso.getByID(usuario.usuarioSistema.id).permiso.authority == 'ROLE_CXP'}">
                                CXP
                            </g:if>

                            <g:if test="${UsuarioSistemaPermiso.getByID(usuario.usuarioSistema.id).permiso.authority == 'ROLE_FINANZAS'}">
                                FINANZAS
                            </g:if>

                            <g:if test="${UsuarioSistemaPermiso.getByID(usuario.usuarioSistema.id).permiso.authority == 'ROLE_USUARIO'}">
                                USUARIO
                            </g:if>
                        </center>
                    </g:if>
                    <g:else>
                        <g:select id="rolUsuario"
                                  name="rol"
                                  style="width: 100px"
                                  from="${Permiso.list()}"
                                  value="${rol}"
                                  optionKey="id"
                                  noSelection="['':'-Seleccione un rol-']"
                                  onchange="${remoteFunction(controller:'solicitudCuenta', update:'superBody',id:usuario?.id, action:'asignarRol',params:'\'role=\' + escape(this.value)')}"/>
                    </g:else>
                </td>
                <td>
                    <g:if test="${!usuario?.usuarioSistema?.enabled}">
                        <form>
                            <g:hiddenField name="id" value="${usuario.id}"/>
                            <center>

                                <g:actionSubmitImage action="resumenActivaUsuario" value="AceptarSolicitud"
                                                     src="${resource(dir:'images/style',file:'palomita.png')}"
                                                     style="border:none;background:transparent;"/>
                            </center>
                        </form>
                    </g:if>

                <td>

                    <g:if test="${usuario?.usuarioSistema?.enabled == false}">
                        PENDIENTE
                    </g:if>
                    <g:elseif test="${usuario?.usuarioSistema?.enabled == true}">
                        ACEPTADO
                    </g:elseif>
                </td>

                <td>
                    <a href="edit?id=${usuario.id}" rel="modal">
                        <img src="${resource(dir: 'images/icons', file: 'pencil.png')}" alt="editar"/>
                    </a>
                </td>

                <td>
                    <center>
                        <g:form>
                            <g:hiddenField name="id" value="${usuario.id}"/>
                            <g:actionSubmitImage id="icono_boton" src="${resource(dir:'images/style', file:'delete.png')}"
                                                 action="delete"
                                                 value="${message(code: 'default.button.delete.label', default: 'Delete')}"
                                                 onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');"/>
                        </g:form>
                    </center>
                </td>

                </td>


                </tr>

            </g:each>
            </tbody>

        </table>


        <div class="pagination">
            <g:paginate total="${usuariosTotal}" action="filtroCuenta"/>
        </div>
        <br/>
    </div>
</div>

</body>
</html>