<%@page import="proyecto.base.security.Permiso;"%>

<g:set var="entityName" value="${message(code: 'solicitudCuenta.label', default: 'SolicitudCuenta')}"/>
<div style="formulario">
    <a href="#" class="close" onclick="javascript:$(document).trigger('close.facebox')">
        <img src="${resource(dir: 'images', file: 'closelabel.png')}" title="close" class="close_image" />
    </a>

    <div class="clear"></div> <!-- End .clear -->
    <div class="content-box"><!-- Start Content Box -->
        <div class="content-box-header">
            <h3><g:message code="solicitud.table.head.header" default="Editar Usuario"/></h3>
        </div>
        <div class="content-box-content">
            <g:if test="${flash.message}">
                <div class="notification information png_bg">
                    <a href="#" class="close"><img src="${resource(dir: 'images/icons', file: 'cross_grey_small.png')}"
                                                   title="Cerrar notificaci&oacute;n" alt="cerrar"/></a>

                    <div>
                        ${flash.message}
                    </div>
                </div>
            </g:if>

            <g:form controller="solicitudCuenta">
                <ol class="property-list tabuladorConcepto">
                    <table border="1">
                        <tr>
                            <td>
                                <b>
                                    <label>
                                        <g:message code="solicitud.table.head.noEmpleado" default="usuario.sistema.nombre"/>
                                        <span class="required-indicator">*</span>
                                    </label>
                                </b>
                            </td>
                        </tr>

                        <tr>
                            <td>
                                <g:textField name="numeroEmpleado"
                                             value="${usuario?.numeroEmpleado}"
                                             style="background: #ffffff"
                                             onblur="return conMayusculas(this)"/>
                            </td>
                        </tr>
                    </table>
                    <br>
                    <table>
                        <tr>
                            <td>
                                <b>
                                    <label>
                                        <g:message code="solicitud.table.head.nombre" default="RFC"/>
                                        <span class="required-indicator">*</span>
                                    </label>
                                </b>
                            </td>

                            <td>
                                <b>
                                    <label>
                                        <g:message code="solicitud.table.head.nombre" default="usuario.sistema.nombre"/>
                                        <span class="required-indicator">*</span>
                                    </label>
                                </b>
                            </td>
                        </tr>

                        <tr>
                            <td>
                                <g:textField name="rfc"
                                             value="${usuario?.rfc}"
                                             style="background: #ffffff"
                                             onblur="return conMayusculas(this)"/>
                            </td>

                            <td>
                                <g:textField name="nombre" size="30"
                                             value="${usuario?.nombre}"
                                             style="background: #ffffff"
                                             onblur="return conMayusculas(this)"/>
                            </td>
                        </tr>
                    </table>
                    <br>
                    <table>
                        <tr>
                            <td>
                                <b>
                                    <label>
                                        <g:message code="solicitud.table.head.email"/>
                                        <span class="required-indicator">*</span>
                                    </label>
                                </b>
                            </td>

                            <td>
                                <b>
                                    <label>
                                        <g:message code="solicitud.table.head.telefono"/>
                                        <span class="required-indicator">*</span>
                                    </label>
                                </b>
                            </td>
                        </tr>

                        <tr>
                            <td>
                                <g:textField name="email"
                                             value="${usuario?.email}"
                                             style="background: #ffffff"/>
                            </td>

                            <td>
                                <g:textField name="telefono"
                                             value="${usuario?.telefono}"
                                             style="background: #ffffff"
                                             onkeypress="return isNumerico(event)"/>
                            </td>
                        </tr>
                    </table>
                    <br>
                    <table>
                        <tr>
                            <td>
                                <b>
                                    <label>
                                        <g:message code="solicitud.table.head.empresa"/>
                                        <span class="required-indicator">*</span>
                                    </label>
                                </b>
                            </td>

                            <td>
                                <b>
                                    <label>
                                        <g:message code="solicitud.table.head.rol"/>
                                        <span class="required-indicator">*</span>
                                    </label>
                                </b>
                            </td>
                        </tr>

                        <tr>
                            <td>
                                <g:select id="empresa" name="empresa.id" from="${mx.org.stirt.catalogos.Empresa.list()}"
                                          optionKey="id" required="" value="${usuario?.usuarioSistema?.empresa?.id}" class="many-to-one"/>
                            </td>

                            <td>
                                <g:if test="${usuario?.usuarioSistema?.enabled == true}">
                                    <g:select id="rol"
                                              name="rol"
                                              from="${mx.org.stirt.login.Permiso.list()}"
                                              value="${rolUsuario?.permiso?.id}"
                                              optionKey="id"/>
                                </g:if>

                                <g:else>
                                    <label>
                                        <g:message code="solicitud.table.head.rol.no.dfinido"/>
                                    </label>
                                </g:else>
                            </td>
                        </tr>
                    </table>
                    <br>
                    <table>
                        <tr>
                            <td colspan="1">
                                <g:message code="solicitud.table.head.Cuenta"/>
                            </td>

                            <td>
                                <g:if test="${usuario?.usuarioSistema?.enabled==true}">
                                    <label style="color: #00B000">
                                       : Aceptado
                                    </label>
                                </g:if>

                                <g:else>
                                    <label style="color: red">
                                        : Pendiente
                                    </label>
                                </g:else>
                            </td>
                        </tr>
                    </table>
                </ol>
                <br>
                    <fieldset class="buttons">
                        <g:hiddenField name="id" value="${usuario?.id}"/>

                        <g:actionSubmit class="button" action="updateUsuario"
                                        value="Aceptar"/>
                    </fieldset>
            </g:form>
        </div>
    </div>
</div>