<script type="text/javascript">
    $(document).ready(function() {
        $("#confirmarNueva").keyup(function(){
            $.ajax({
                type: "POST",
                url: "${createLink(action: 'validaContrasenia', controller:'usuario')}",
                data: {
                    'contraseniaNueva': $("#contraseniaNueva").val(),
                    'confirmarNueva': $("#confirmarNueva").val() // <-- the $ sign in the parameter name seems unusual, I would avoid it
                },
                success : function(response) {
                    if(response.exito){
                        $("#error").hide()
                        $("#mensaje").show()
                        $("#aceptar").removeAttr("disabled");
                        $("#mensaje").html(response.exito)
                    }else{
                        $("#error").show()
                        $("#mensaje").hide()
                        $("#aceptar").attr("disabled", "disabled");
                        $("#error").html(response.error)
                    }
                }
            });
        });
    });

    function limpiarCamposContrasenia(){
        $("#error").hide()
        $("#mensaje").hide()
        $("#contraseniaActual").val('')
        $("#contraseniaNueva").val('')
        $("#confirmarNueva").val('')
    }
</script>

<!-- Modal -->
<div id="formContrasena" class="modal hide fade span4" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true" style="">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h3 id="modalLabel"><g:message code="cambiar.contrasenia.titulo.form" default="Cambiar contrase\u00f1a"/> </h3>
        <i class="icon-user"></i>
        <sec:loggedInUserInfo field="username"></sec:loggedInUserInfo>
    </div>
<g:formRemote name="cambiarContraseniaFrm" update="mensajes"
              url="[controller: 'usuario', action: 'actualizarContrasenia']"  onSuccess="limpiarCamposContrasenia()" autocomplete="off">
    <div class="modal-body">
        <g:render template="/common/mensajes"/>

        <span><label><g:message code="cambiar.contrasenia.actual" default="Contrase\u00f1a actual"/></label></span>
        <g:passwordField id="contraseniaActual" name="contraseniaActual" class="input-medium"/>                                             <br/>
        <span><label><g:message code="cambiar.contrasenia.nueva" default="Contrase\u00f1a nueva"/></label></span>
        <g:passwordField id="contraseniaNueva" name="contraseniaNueva" class="input-medium"/>                                             <br/>
        <span><label><g:message code="cambiar.contrasenia.repetir.nueva" default="Confirmar nueva"/></label></span>
        <g:passwordField id="confirmarNueva" name="confirmarNueva" class="input-medium" />
        <small>
            <span id="mensaje" class="text-info"></span>
            <span id="error" class="text-error"></span>
        </small>

    </div>
    <div class="modal-footer">
        <button class="btn" data-dismiss="modal" aria-hidden="true">
            <g:message code="btn.close.label" default="Cerrar"/>
        </button>
        <button id="aceptar" class="btn btn-primary" disabled="disabled" type="submit">
            <g:message code="btn.save.label" default="Guardar"/>
        </button>
    </div>
    </g:formRemote>
</div>


