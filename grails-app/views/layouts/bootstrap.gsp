<!DOCTYPE html>
<html lang="en">
<head>

    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title><g:layoutTitle default="Grails"/></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="${resource(dir: 'images', file: 'favicon.ico')}" type="image/x-icon">

    <link rel="stylesheet" href="${resource(dir: 'css', file: 'bootstrap.css')}" type="text/css">
    <style>
        body {
            padding-top: 60px; /* 60px to make the container go all the way to the bottom of the topbar */
        }
        .table-button {
            border: none;
            background: transparent;
        }
    </style>
    <r:require modules="bootstrap"/>
    <!--link rel="stylesheet" href="${resource(dir: 'css', file: 'bootstrap-responsive.css')}" type="text/css"-->

    <g:javascript library="jquery"/>
    <r:require module="jquery-ui"/>

    <g:layoutHead/>
    <r:layoutResources />
</head>

<body>
<g:render template="/inicio/menu"/>
<div class="container">
    <g:layoutBody/>
</div> <!-- /container -->


<g:javascript library="application"/>
<g:javascript library="bootstrap"/>



<r:layoutResources />

</body>
</html>
