<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="bootstrap_fluid"/>
    <title><g:message code="titulo.pantalla.inicio" default="Pantalla de inicio"/> </title>
</head>
<body>
    <div class="hero-unit">
        <h1><g:message code="titulo.pantalla.inicio" default="Pantalla de inicio"/> </h1>
        <g:message code="contenido.pantalla.inicio" />

    </div>


</body>
</html>