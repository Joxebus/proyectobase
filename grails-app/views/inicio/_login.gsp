<form id="frmLogin" action='${request.contextPath}/j_spring_security_check' method='POST' autocomplete='off' class="navbar-form pull-right">
    <i class="icon-user icon-white"></i>
    <g:textField class="span2" id='username' name="j_username" value=""
                 placeholder="${message(code: 'username.placeholder', default: 'usuario')}"/>
    <g:passwordField class="span2" id='password' name="j_password" value=""
                     placeholder="${message(code: 'password.placeholder', default: 'contrase\u00f1a')}"/>
    <button type="submit" class="btn">Sign in</button>
</form>
<script type='text/javascript'>
    <!--
    (function () {
        document.forms['loginForm'].elements['j_username'].focus();
    })();
    // -->
</script>
