<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="bootstrap_fluid"/>
    <title><g:message code="titulo.pantalla.contacto" default="Contactanos"/></title>

</head>

<body>
<div class="hero-unit">
    <h1><g:message code="titulo.pantalla.contacto" default="Contactanos"/></h1>
    <g:message code="contenido.pantalla.contacto"/>

    <div class="row">
        <div class="span4">
            <fieldset>
                <legend><g:message code="titulo.formulario.contato" default="Contáctanos"/></legend>
                <form method="post" class="form-horizontal">
                    <div class="control-group">
                        <label class="control-label"><g:message code="label.nombre" default="Nombre"/></label>

                        <div class="controls">
                            <g:textField name="nombre" placeholder="nombre"/>

                        </div>
                    </div>

                    <div class="control-group">
                        <label class="control-label">
                            <i class="icon-envelope"></i><g:message code="label.correo" default="Correo"/></label>

                        <div class="controls">
                            <g:textField name="correo" placeholder="correo"/>
                        </div>
                    </div>

                    <div class="control-group">
                        <label class="control-label"><g:message code="label.comentario" default="Comentario"/></label>

                        <div class="controls">
                            <g:textArea name="comentario" rows="4" cols="50"/>
                        </div>
                    </div>

                    <div class="control-group">
                        <div class="controls">
                            <label class="checkbox">
                                <g:checkBox name="copia" class="checkbox"/> <g:message code="label.enviar.copia.correo" default="Recibir una copia"/>
                            </label>

                            <g:actionSubmit value="Enviar" class="btn btn-primary"/>
                        </div>
                    </div>

                </form>
            </fieldset>
        </div>
    </div>
</div>
</body>
</html>