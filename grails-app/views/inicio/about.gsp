<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="bootstrap_fluid"/>
    <title><g:message code="about.page.title" default="Acerca de..."/></title>
</head>
<body>
<div class="hero-unit">
    <h1><g:message code="titulo.pantalla.acerca.de" default="Acerca de..."/> </h1>
    <g:message code="contenido.pantalla.acerca.de"/>
    <p>
       <a href="#" id="tooltip" rel="tooltip" title="tooltip text">lorem ipsum</a>
    </p>
</div>
</body>
</html>