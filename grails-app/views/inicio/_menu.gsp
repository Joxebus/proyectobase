<div class="navbar navbar-inverse navbar-fixed-top">
    <div class="navbar-inner">
        <div class="container">
            <button type="button" class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="brand" href="${createLink(uri: '/')}"><g:meta name="app.name"/></a>
            <div class="nav-collapse collapse">
                <ul class="nav">
                    <li class="active"><a href="${createLink(uri: '/')}">Inicio</a></li>
                <sec:ifLoggedIn>
                    <sec:ifAnyGranted roles="ROLE_ADMIN">
                    <li class="controller"><a href="#">Menu 1</a></li>
                    <li class="controller"><a href="#">Menu 2</a></li>
                    <li class="controller"><a href="#">Menu 3</a></li>
                    <li class="controller"><a href="#">Menu 4</a></li>

                    </sec:ifAnyGranted>
                </sec:ifLoggedIn>
                    <li><a href="${createLink(uri: '/about')}">About</a></li>
                    <li><a href="${createLink(uri: '/contact')}">Contact</a></li>

                </ul>
                <sec:ifNotLoggedIn>
                    <g:render template="/inicio/login"/>
                </sec:ifNotLoggedIn>
                <sec:ifLoggedIn>
                <ul class="nav pull-right">
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <i class="icon-user icon-white"></i>
                            <sec:loggedInUserInfo field="username"></sec:loggedInUserInfo>
                        </a>
                        <ul class="dropdown-menu">
                            <!--li class="divider"></li-->
                            <li><g:link controller="logout">Cerrar sesi&oacute;n <i class="icon-off"></i></g:link></li>
                            <li><a href="#formContrasena" role="button" data-toggle="modal">Cambiar contrase&ntilde;a</a></li>
                        </ul>
                    </li>

                </ul>
                </sec:ifLoggedIn>


            </div><!--/.nav-collapse -->

        </div>

    </div>
</div>
<g:render template="/login/cambiarContrasenia"/>