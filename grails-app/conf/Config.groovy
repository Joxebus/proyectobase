// locations to search for config files that get merged into the main config;
// config files can be ConfigSlurper scripts, Java properties files, or classes
// in the classpath in ConfigSlurper format

// grails.config.locations = [ "classpath:${appName}-config.properties",
//                             "classpath:${appName}-config.groovy",
//                             "file:${userHome}/.grails/${appName}-config.properties",
//                             "file:${userHome}/.grails/${appName}-config.groovy"]

// if (System.properties["${appName}.config.location"]) {
//    grails.config.locations << "file:" + System.properties["${appName}.config.location"]
// }

// Here you can put your local file system for storage files
local.url.files = '/tmp/file_manager/'
local.max.size.files =  5242880 // 5MB per file

// Mail plugin configuration
mail.default.subject.register='Registro de cuenta Proyecto Base'
mail.default.subject.recover='Recuperaci\u00f3n de contrase\u00f1a Proyecto Base'
mail.default.subject.activate='Activaci\u00f3n de cuenta Proyecto Base'
mail.default.no.reply='no-reply@proyectobase.com'

// Twitter Bootstrap
grails.plugins.twitterbootstrap.fixtaglib = true

grails.project.groupId = appName // change this to alter the default package name and Maven publishing destination
grails.mime.file.extensions = true // enables the parsing of file extensions from URLs into the request format
grails.mime.use.accept.header = false
grails.mime.types = [
    all:           '*/*',
    atom:          'application/atom+xml',
    css:           'text/css',
    csv:           'text/csv',
    form:          'application/x-www-form-urlencoded',
    html:          ['text/html','application/xhtml+xml'],
    js:            'text/javascript',
    json:          ['application/json', 'text/json'],
    multipartForm: 'multipart/form-data',
    rss:           'application/rss+xml',
    text:          'text/plain',
    xml:           ['text/xml', 'application/xml'],
    doc:           'application/msword',
    docx:          'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
    xls:           'application/vnd.ms-excel',
    xlsx:          'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
    pdf:           'application/pdf',
    txt:           'text/plain',
    mp3:           'audio/mpeg',
    wav:           'audio/wav',
    wma:           'audio/x-ms-wma',
    ogg:           'audio/x-ms-wma',
    jpe:           'image/jpeg',
    jpg:           'image/jpeg',
    jpeg:          'image/jpeg',
    gif:           'image/gif',
    png:           'image/png',
    bmp:           'image/bmp'
]

// URL Mapping Cache Max Size, defaults to 5000
//grails.urlmapping.cache.maxsize = 1000

// What URL patterns should be processed by the resources plugin
grails.resources.adhoc.patterns = ['/images/*', '/css/*', '/js/*', '/plugins/*']

// The default codec used to encode data with ${}
grails.views.default.codec = "none" // none, html, base64
grails.views.gsp.encoding = "UTF-8"
grails.converters.encoding = "UTF-8"
// enable Sitemesh preprocessing of GSP pages
grails.views.gsp.sitemesh.preprocess = true
// scaffolding templates configuration
grails.scaffolding.templates.domainSuffix = 'Instance'

// Set to false to use the new Grails 1.2 JSONBuilder in the render method
grails.json.legacy.builder = false
// enabled native2ascii conversion of i18n properties files
grails.enable.native2ascii = true
// packages to include in Spring bean scanning
grails.spring.bean.packages = []
// whether to disable processing of multi part requests
grails.web.disable.multipart=false

// request parameters to mask when logging exceptions
grails.exceptionresolver.params.exclude = ['password']

// configure auto-caching of queries by default (if false you can cache individual queries with 'cache: true')
grails.hibernate.cache.queries = false

environments {
    development {
        grails.logging.jul.usebridge = true
    }
    production {
        grails.logging.jul.usebridge = false
        // TODO: grails.serverURL = "http://www.changeme.com"
    }
}

// log4j configuration
log4j = {
    // Example of changing the log pattern for the default console appender:
    //
    //appenders {
    //    console name:'stdout', layout:pattern(conversionPattern: '%c{2} %m%n')
    //}
    appenders {
        console name:'stdout', layout:pattern(conversionPattern: '[%d{yyyy-MM-dd HH:mm:ss}] %c{2} - %m%n')
        //rollingFile name:'archivo', maxFileSize:'10MB', file:"D:/logs/proyecto_base_debug_out.log", layout:pattern(conversionPattern: '[%d{yyyy-MM-dd HH:mm:ss}] %c{5} - %m%n')
    }

    error  'org.codehaus.groovy.grails.web.servlet',        // controllers
           'org.codehaus.groovy.grails.web.pages',          // GSP
           'org.codehaus.groovy.grails.web.sitemesh',       // layouts
           'org.codehaus.groovy.grails.web.mapping.filter', // URL mapping
           'org.codehaus.groovy.grails.web.mapping',        // URL mapping
           'org.codehaus.groovy.grails.commons',            // core / classloading
           'org.codehaus.groovy.grails.plugins',            // plugins
           'org.codehaus.groovy.grails.orm.hibernate',      // hibernate integration
           'org.springframework',
           'org.hibernate',
           'net.sf.ehcache.hibernate'

    debug 'grails.app.controllers'
    debug 'grails.app.services'
    debug 'grails.app.domain'
    debug 'grails.app.conf'
}

// Configuraciones para Spring Security:
grails.plugins.springsecurity.userLookup.userDomainClassName = 'proyecto.base.security.UsuarioSistema'
grails.plugins.springsecurity.authority.className = 'proyecto.base.security.Permiso'
grails.plugins.springsecurity.userLookup.authorityJoinClassName = 'proyecto.base.security.UsuarioSistemaPermiso'
grails.plugins.springsecurity.password.algorithm = 'MD5'

grails.plugins.springsecurity.securityConfigType = "InterceptUrlMap"
grails.plugins.springsecurity.interceptUrlMap = [

        // Ejemplo de como poner.
        // /urlController/**         ['RolName1','RolName2']

        '/inicio/**':         ['IS_AUTHENTICATED_ANONYMOUSLY'],
        '/js/**':             ['IS_AUTHENTICATED_ANONYMOUSLY'],
        '/css/**':            ['IS_AUTHENTICATED_ANONYMOUSLY'],
        '/images/**':         ['IS_AUTHENTICATED_ANONYMOUSLY'],

        '/login/**':          ['IS_AUTHENTICATED_ANONYMOUSLY'],
        '/logout/**':         ['IS_AUTHENTICATED_ANONYMOUSLY'],

        '/common/**':         ['IS_AUTHENTICATED_FULLY','ROLE_ADMIN'],

]
