modules = {
    application {
        resource url:'js/application.js'
    }

    bootstrap {
        resource url:'js/bootstrap.js'
    }

    jquery {
        resource url:'js/jquery-1.7.2.js'
    }

    utilClientes {
        resource url:'js/autoComplete/utilClientes.js'
    }

    ventaUtils {
        resource url:'js/ventas/ventaUtils.js'
    }
}