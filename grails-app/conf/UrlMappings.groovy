class UrlMappings {

	static mappings = {
		"/$controller/$action?/$id?"{
			constraints {
				// apply constraints here
			}
		}

		"/"(view:"inicio/index")
        "/about"(view: "inicio/about")
        "/contact"(view: "inicio/contact")
		"500"(view:'/error')
	}
}
