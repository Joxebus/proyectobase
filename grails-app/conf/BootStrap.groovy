import proyecto.base.security.Permiso
import proyecto.base.security.UsuarioSistema
import proyecto.base.security.UsuarioSistemaPermiso

class BootStrap {

    def init = { servletContext ->


        def rolAdmin

        if(!Permiso.count()){
            rolAdmin = new Permiso(
                    authority: 'ROLE_ADMIN',
                    tipoUsuario: 'ADMINISTRADOR').save(failOnError: true)
        }

        if(!UsuarioSistema.count())  {
            def adminUser = new UsuarioSistema(
                    nombre: 'Administrador',
                    apellidoPaterno: 'Sistema',
                    apellidoMaterno: 'Base',
                    password: "admin",
                    username: 'admin@admin.com',
                    tipoUsuario: rolAdmin?.tipoUsuario,
                    enabled: true,
                    accountExpired: false,
                    accountLocked: false,
                    passwordExpired: false,

            ).save(failOnError: true)
            log.debug("Creando usuario Administrador")

            UsuarioSistemaPermiso.create adminUser, rolAdmin, true
            1.upto(10){
                new UsuarioSistema(
                        nombre: 'Usuario '+it,
                        apellidoPaterno: 'Sistema',
                        apellidoMaterno: 'Base',
                        password: "user$it",
                        username: "user$it@user.com",
                        tipoUsuario: '',
                        enabled: false,
                        accountExpired: false,
                        accountLocked: false,
                        passwordExpired: false,

                ).save(failOnError: true)
            }

        }

    }
    def destroy = {


    }
}
