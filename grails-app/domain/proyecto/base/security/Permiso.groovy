package proyecto.base.security

class Permiso {

    String authority
    String tipoUsuario

	static mapping = {
		cache true
        datasource 'ALL'
	}

	static constraints = {
		authority blank: false, unique: true
        tipoUsuario blank: false, nullable: false
	}

    String toString(){
        tipoUsuario
    }
}
