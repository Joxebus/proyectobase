package proyecto.base.security



class UsuarioSistema {

    transient springSecurityService
	transient String captcha

    // Datos del usuario
    String nombre
    String apellidoPaterno
    String apellidoMaterno
    String username
    String password
	Boolean enabled
	Boolean accountExpired
	Boolean accountLocked
	Boolean passwordExpired

    String tipoUsuario

    Integer usuarioModifica
	Integer estadoLogico
	Date dateCreated
	Date lastUpdated



	static constraints = {
		username (email:true, blank:false, unique:true, maxSize:60)
		password (blank:false, maxSize:50)
        tipoUsuario nullable: true, blank:true
        nombre  (blank:false, maxSize:60)
        apellidoPaterno  (blank:false, maxSize:60)
        apellidoMaterno  (blank:false, maxSize:60)
		enabled (nullable:false)
		usuarioModifica(nullable:true)
		estadoLogico(nullable :true)
        captcha(nullable:true)
	}


	Set<Permiso> getAuthorities() {
		UsuarioSistemaPermiso.findAllByUsuarioSistema(this).collect { it.permiso } as Set
	}

	def beforeInsert() {
		encodePassword()
		estadoLogico = 1
	}

	def beforeUpdate() {
		if (isDirty('password')) {
			encodePassword()
		}
	}

	protected void encodePassword() {
		password = springSecurityService.encodePassword(password)
	}

    static mapping = {
        datasource 'ALL'
    }

    String toString(){
        "$nombre ${apellidoPaterno?:''} ${apellidoMaterno?:''}"
    }
}
