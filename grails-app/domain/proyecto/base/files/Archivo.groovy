package proyecto.base.files

import proyecto.base.util.FileProperties

class Archivo {

    String  ubicacionLocal
    String  contentType
    String  extension
    String  nombre
    Integer tamano

    static constraints = {
        tamano max:FileProperties.MB*5
    }

    transient Integer getTamano(){
        return tamano?:0/FileProperties.MB
    }

    String toString(){
        nombre.replaceAll(' ','_')+"."+extension
    }
}
