PROYECTO BASE TWITTER BOOTSTRAP

El presente proyecto continene la estructura base para comenzar a contruir una aplicaci�n en Grails.

Se compone de la siguiente estructura:

- Grails 2.2.4
- ImageTools (Herramientas para redimencionar imagenes)
- Twitter Bootstrap 2.3.2 (Los templates instalados ya utilizan �ste estilo)
- Rendering (Para generar archivos PDF a partir de GSPs)
- Spring Security 1.2.7
- JQuery 1.7.2
- JQUery-UI 1.8.24

Algunos de los modulos que se �stan desarrollando y que �stan en fase de pruebas son:

- Manejo de archivos en File System
- ABC Usuarios del sistema
- Asignaci�n de roles a los usuarios

Pantallas desarrolladas:

- Inicio
- Acerca de
- Contacto
- ABC Usuarios
- Listado de usuarios
- Asignacion de rol
- Recuperacion de contrase�a
- Env�o de notificaciones

A�n se encuentran en desarrollo algunas de las funcionalidades dado que se pretende dejar una estructura base estable.
