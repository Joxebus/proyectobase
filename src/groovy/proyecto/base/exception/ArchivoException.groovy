package proyecto.base.exception

/**
 * Created with IntelliJ IDEA.
 * User: user
 * Date: 24/10/13
 * Time: 06:16 PM
 */
class ArchivoException extends RuntimeException {

    public ArchivoException(String mensaje){
        super(mensaje)
    }
}
