package proyecto.base.util

/**
  * User: Jorge Omar Bautista Valenzuela
 * Date: 27/07/12
 * Time: 16:13
 * email: jbautista@sintelti.com.mx
 *
 * Esta clase contiene propiedades utilitarias para realizar validaciones.
 */
class FileProperties {

    /**
     * Unidades de medida en MB.
     */
    public static final Integer MB = 1024*1024   // 1MB
    public static final Integer MAX_IMAGE_SIZE = 5*MB // 5MB en bytes
    public static final Integer MAX_FILE_SIZE = 10*MB // 10MB en bytes

    /**
     * Expresiones regulares para validaciones.
     */
    public static final String TELEFONO = /(\+[0-9]+-)*([0-9]{3}-){2}[0-9]{4}/
    public static final String NSS = /[0-9]{12}/
    public static final String CURP = /[A-Z]{4}[0-9]{6}(H|M){1}[A-Z]{2}([B-D]|[F-H]|[J-N]|[P-T]|[V-Z]){3}[0-9]{2}/
    public static final String RFC_FISICAS = /[A-Z]{4}[0-9]{6}([A-Z0-9]{3})*/
    public static final String RFC_MORALES = /[A-Z]{3}[0-9]{6}([A-Z0-9]{3})*/
}
