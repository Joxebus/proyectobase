@artifact.package@class @artifact.name@ {

    // Fechas de para el control de modificaciones
    Date dateCreated
    Date lastUpdated

    static constraints = {
    }
}
