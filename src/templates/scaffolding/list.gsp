<% import grails.persistence.Event %>
<%=packageName%>
<!doctype html>
<html>
	<head>
		<meta name="layout" content="bootstrap_fluid">
		<g:set var="entityName" value="\${message(code: '${domainClass.propertyName}.label', default: '${className}')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<div class="row-fluid">
            <div class="span3">
                <div class="well sidebar-nav">
                    <ul class="nav nav-list">
                        <li class="nav-header"><g:message code="default.actions.label" default="Acciones" /></li>
                        <li><g:link class="create" action="create">
                            <i class="icon-plus"></i><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			        </ul>
                </div>
            </div>


		<div id="list-${domainClass.propertyName}" class="span9" role="main">
			<h1><g:message code="default.list.label" args="[entityName]" /></h1>
			<g:if test="\${flash.message}">
			<div class="alert alert-success" role="status">\${flash.message}</div>
			</g:if>
			<table class="table table-hover">
				<thead>
					<tr>
					<%  excludedProps = Event.allEvents.toList() << 'id' << 'version' << 'dateCreated' << 'lastUpdated'
						allowedNames = domainClass.persistentProperties*.name
						props = domainClass.properties.findAll { allowedNames.contains(it.name) && !excludedProps.contains(it.name) && it.type != null && !Collection.isAssignableFrom(it.type) }
						Collections.sort(props, comparator.constructors[0].newInstance([domainClass] as Object[]))
						props.eachWithIndex { p, i ->
							if (i < 6) {
								if (p.isAssociation()) { %>
						<th><g:message code="${domainClass.propertyName}.${p.name}.label" default="${p.naturalName}" /></th>
					<%      } else { %>
						<g:sortableColumn property="${p.name}" title="\${message(code: '${domainClass.propertyName}.${p.name}.label', default: '${p.naturalName}')}" />
					<%  }   }   } %>
                        <th class="sortable" width="10px">Acciones</th>
					</tr>
				</thead>
				<tbody>
				<g:each in="\${${propertyName}List}" status="i" var="${propertyName}">
					<tr class="\${(i % 2) == 0 ? 'even' : 'odd'}">
					<%  props.eachWithIndex { p, i ->
							if (i == 0) { %>
						<td><g:link action="show" id="\${${propertyName}.id}">\${fieldValue(bean: ${propertyName}, field: "${p.name}")}</g:link></td>
					<%      } else if (i < 6) {
								if (p.type == Boolean || p.type == boolean) { %>
						<td><g:formatBoolean boolean="\${${propertyName}.${p.name}}" /></td>
					<%          } else if (p.type == Date || p.type == java.sql.Date || p.type == java.sql.Time || p.type == Calendar) { %>
						<td><g:formatDate date="\${${propertyName}.${p.name}}" /></td>
					<%          } else { %>
						<td>\${fieldValue(bean: ${propertyName}, field: "${p.name}")}</td>
					<%  }   }   } %>
                        <td><!-- Icons -->
                            <g:form>
                                <g:hiddenField name="id" value="\${${propertyName}.id}"/>
                                <g:actionSubmitImage class="table-button" value="\${message(code: 'global.edit.label', default: 'Editar')}" src="\${resource(dir: 'images/icons', file: 'pencil.png')}" action="edit" formnovalidate=""/>
                                <g:actionSubmitImage class="table-button" value="\${message(code: 'global.delete.label', default: 'Eliminar')}" src="\${resource(dir: 'images/icons', file: 'cross.png')}" action="delete" formnovalidate="" onclick="return confirm('\${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');"/>
                            </g:form>
                        </td>
					</tr>
				</g:each>
				</tbody>
			</table>

			<g:paginate total="\${${propertyName}Total}" />

		</div>
        </div>
	</body>
</html>
